using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorDoors : MonoBehaviour
{
    public GameObject leftDoor;
    public GameObject rightDoor;

    public bool doorsClosed;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void CloseDoors()
    {
        if(doorsClosed == false)
        {
            rightDoor.transform.position += new Vector3(-0.13f, 0f, 0f);
            leftDoor.transform.position += new Vector3(0.13f, 0f, 0f);
            doorsClosed = true;
        }
    }

    public void OpenDoors()
    {
        rightDoor.transform.position += new Vector3(0.13f, 0f, 0f);
        leftDoor.transform.position += new Vector3(-0.13f, 0f, 0f);
        doorsClosed = false;
    }
}
