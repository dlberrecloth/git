using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flicker : MonoBehaviour
{
    private float flickerCountdown;

    public float flickerChance;
    private float delayTime;

    private Light _light;

    // Start is called before the first frame update
    void Start()
    {
        _light = this.gameObject.GetComponent<Light>();
        flickerCountdown = Random.Range(0, 20);
    }

    // Update is called once per frame
    void Update()
    {
        flickerCountdown -= 1 * Time.deltaTime;

        if(flickerCountdown <= 0)
        {
            flickerChance = Random.Range(0, 100);

            if(flickerChance >= 80)
            {
                _light.intensity = 0;
                delayTime = Random.Range(1, 3);
                Invoke("FlickLightOn", delayTime);
                flickerCountdown = Random.Range(10f, 30f);
            }
            else
            {
                flickerCountdown = Random.Range(10f, 30f);
            }
        }
    }

    void FlickLightOn()
    {
        _light.intensity = 0.7f;
    }
}
