using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveScript : MonoBehaviour
{
    public CharacterController controller;

    public bool controlsEnabled = true;
    bool enableElevatorTimer = false;
    public float elevatorTimer;

    float runSpeed = 2f;
    float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;

    private Animator _animator;

    public bool inElevator = false;
    public bool inRoom = false;

    public float currentFloor = 5f;
    float topFloor = 10f;
    float bottomFloor = 1f;

    float horizontal;

    public GameObject _room;
    public GameObject Elevators;

    public int health = 3;
    public float damageCooldown = 3;
    public bool canBeDamaged = true;

    public bool leftSide;
    public bool rightSide;

    public GameObject _ghost;
    public float ghostChance;
    public float numberOfGhosts;

    public GameObject health1;
    public GameObject health2;
    public GameObject health3;

    private AudioSource _audioSource;

    public AudioClip hitSound;
    public AudioClip doorSound;
    public AudioClip elevatorButtonSound;
    public AudioClip elevatorArrivedSound;
    public AudioClip pickupSound;

    public GameObject[] hauntedIcons;
    public GameObject chosenIcon;
    public int iconArrayInd = 0;

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        _audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        Vector3 direction = new Vector3(horizontal, 0f, 0f);

        if (Input.GetKeyDown("w"))
        {
            if(inElevator == true)
            {
                if(currentFloor < topFloor)
                {
                    Elevators.GetComponent<ElevatorDoors>().CloseDoors();
                    controlsEnabled = false;
                    transform.position += new Vector3(0, 1, 0);
                    currentFloor += 1;
                    elevatorTimer = 2;
                    enableElevatorTimer = true;
                    _audioSource.PlayOneShot(elevatorButtonSound, 0.1f);
                }
            }

            if(inRoom == true)
            {
                if(_room.GetComponent<Box>().isOpened == false)
                {
                    _room.GetComponent<Box>().OpenRoom();
                    GhostOrNoGhost();
                    _audioSource.PlayOneShot(doorSound, 0.1f);
                }
                else
                {
                    if (_room.GetComponent<Box>().hasHauntedItem)
                    {
                        _room.GetComponent<Box>().OpenRoom();
                        GhostOrNoGhost();
                        _audioSource.PlayOneShot(pickupSound, 0.1f);
                        _room.GetComponent<Box>().hasHauntedItem = false;
                        chosenIcon = hauntedIcons[iconArrayInd];
                        Destroy(chosenIcon);
                        iconArrayInd += 1;
                    }
                }
            }
        }

        if (Input.GetKeyDown("s"))
        {
            if (inElevator == true)
            {
                if(currentFloor > bottomFloor)
                {
                    Elevators.GetComponent<ElevatorDoors>().CloseDoors();
                    controlsEnabled = false;
                    transform.position -= new Vector3(0, 1, 0);
                    currentFloor -= 1;
                    elevatorTimer = 2;
                    enableElevatorTimer = true;
                    _audioSource.PlayOneShot(elevatorButtonSound, 0.1f);
                }
            }
        }

        if(enableElevatorTimer == true)
        {
            elevatorTimer -= Time.deltaTime;
            {
                if (elevatorTimer <= 0)
                {
                    Elevators.GetComponent<ElevatorDoors>().OpenDoors();
                    controlsEnabled = true;
                    enableElevatorTimer = false;
                    _audioSource.PlayOneShot(elevatorArrivedSound, 0.1f);
                }
            }
        }

        if (direction.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(direction.x, 0f) * Mathf.Rad2Deg;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            transform.rotation = Quaternion.Euler(0f, angle, 0f);

            if(controlsEnabled == true)
            {
                controller.Move(direction * runSpeed * Time.deltaTime);
                _animator.SetBool("isMoving", true);
            }
        }
        else
        {
            _animator.SetBool("isMoving", false);
        }

        if(canBeDamaged == false)
        {
            damageCooldown -= 1 * Time.deltaTime;

            if(damageCooldown <= 0)
            {
                canBeDamaged = true;
                damageCooldown = 3;
            }
        }
    }

    public void CheckHealth()
    {
        if(health <= 0)
        {
            GameObject game = GameObject.FindGameObjectWithTag("GameController");
            game.GetComponent<Game>().NoHealth();
        }
    }

    public void GhostOrNoGhost()
    {
        ghostChance = UnityEngine.Random.Range(0, 100);

        if(ghostChance >= 70)
        {
            SpawnGhost();
        }
    }
    
    public void SpawnGhost()
    {
        float floor = UnityEngine.Random.Range(-1, 1);

        if (leftSide == true)
        {
            Instantiate(_ghost, transform.position + new Vector3(2f, floor, 0f), transform.rotation);
            numberOfGhosts += 1;
        }

        if(rightSide == true)
        {
            Instantiate(_ghost, transform.position + new Vector3(-2f, floor, 0f), transform.rotation);
            numberOfGhosts += 1;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Elevator")
        {
            inElevator = true;
        }

        if(other.gameObject.tag == "Room")
        {
            inRoom = true;
            _room = other.gameObject;
        }

        if(other.gameObject.tag == "Ghost")
        {
            if(canBeDamaged == true)
            {
                _audioSource.PlayOneShot(hitSound, 0.1f);

                if (health == 1)
                {
                    health1.SetActive(false);
                    health -= 1;
                    canBeDamaged = false;
                    CheckHealth();
                }

                if (health == 2)
                {
                    health2.SetActive(false);
                    health -= 1;
                    canBeDamaged = false;
                    CheckHealth();
                }

                if (health == 3)
                {
                    health3.SetActive(false);
                    health -= 1;
                    canBeDamaged = false;
                    CheckHealth();
                }
            }
        }

        if(other.gameObject.tag == "LeftInd")
        {
            rightSide = false;
            leftSide = true;
        }

        if(other.gameObject.tag == "RightInd")
        {
            leftSide = false;
            rightSide = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == "Elevator")
        {
            inElevator = false;
        }

        if (other.gameObject.tag == "Room")
        {
            inRoom = false;
        }
    }
}
