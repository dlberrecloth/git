using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    private Board _board;
    private UI _ui;
    private double _gameStartTime;
    private bool _gameInProgress;
    public int hauntedCollected;

    public void OnClickedNewGame()
    {
        if (_board != null)
        {
            _board.RechargeBoxes();
        }

        if (_ui != null)
        {
            _ui.HideMenu();
            _ui.ShowGame();
        }
    }

    public void OnClickedExit()
    {
#if !UNITY_EDITOR
        Application.Quit();
#endif
    }

    public void OnClickedReset()
    {
        SceneManager.LoadScene(0);
    }

    private void Awake()
    {
        _board = transform.parent.GetComponentInChildren<Board>();
        _ui = transform.parent.GetComponentInChildren<UI>();
        _gameInProgress = false;
    }

    private void Start()
    {
        if (_board != null)
        {
            _board.Setup(BoardEvent);
        }

        if (_ui != null)
        {
            _ui.ShowMenu();
        }
    }

    private void Update()
    {
        if(_ui != null)
        {
            _ui.UpdateTimer(_gameInProgress ? Time.realtimeSinceStartupAsDouble - _gameStartTime : 0.0);
        }
    }

    public void CheckWin()
    {
        if(hauntedCollected >= 10)
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            player.SetActive(false);

            _ui.HideGame();
            _ui.ShowResult(success: true);
        }
    }

    public void NoHealth()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        player.SetActive(false);

        _ui.HideGame();
        _ui.ShowResult(success: false);
    }

    private void BoardEvent(Board.Event eventType)
    {
        if(eventType == Board.Event.ClickedDanger && _ui != null)
        {

        }

        if (eventType == Board.Event.Win && _ui != null)
        {

        }

        if (!_gameInProgress)
        {
            _gameInProgress = true;
            _gameStartTime = Time.realtimeSinceStartupAsDouble;
        }
    }
}
