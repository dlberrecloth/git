using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostScript : MonoBehaviour
{
    private float runSpeed = 0.5f;
    public float lifeTime;

    public bool leftIfTrue;

    public float decisionTimer;
    public int decision;

    public bool wait;

    public bool canGoRight;
    public bool canGoLeft;

    public Vector3 leftDirection;
    public Vector3 rightDirection;

    // Start is called before the first frame update
    void Start()
    {
        lifeTime = Random.Range(45, 240);
        
        leftDirection = new Vector3(0, -180, 0);
        rightDirection = new Vector3(0, 90, 0);

        decisionTimer = Random.Range(0, 5);

        canGoLeft = true;
        canGoRight = true;

        Brain();
    }

    // Update is called once per frame
    void Update()
    {
        lifeTime -= 1 * Time.deltaTime;

        if(lifeTime <= 0)
        {
            Destroy(this.gameObject);
        }
        
        if(wait == false)
        {
            if (leftIfTrue == false)
            {
                if(canGoRight == true)
                {
                    transform.rotation = Quaternion.Euler(rightDirection);
                    transform.position += Vector3.right * runSpeed * Time.deltaTime;
                }
                else
                {
                    Brain();
                }
            }
            
            if(leftIfTrue == true)
            {
                if(canGoLeft == true)
                {
                    transform.rotation = Quaternion.Euler(leftDirection);
                    transform.position += Vector3.left * runSpeed * Time.deltaTime;
                }
                else
                {
                    Brain();
                }
            }
        }
    }

    void Brain()
    {
        decision = Random.Range(1, 4);

        if(decision == 3)
        {
            leftIfTrue = false;
        }

        if(decision == 2)
        {
            leftIfTrue = true;
        }

        if(decision == 1)
        {
            wait = true;
        }
        else
        {
            wait = false;
        }
    }

    void FixedUpdate()
    {
        if(decisionTimer >= 0)
        {
            decisionTimer -= 1f * Time.deltaTime;
        }
        else
        {
            Brain();
            decisionTimer = Random.Range(0, 5);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "GhostZoneL")
        {
            canGoLeft = false;
            Brain();
        }

        if (other.gameObject.tag == "GhostZoneR")
        {
            canGoRight = false;
            Brain();
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "GhostZoneL")
        {
            canGoLeft = true;
        }

        if (other.gameObject.tag == "GhostZoneR")
        {
            canGoRight = true;
        }
    }
}
